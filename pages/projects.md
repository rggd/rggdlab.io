<!-- 3 MENU_ENTRY=Projects -->
# Projects



## MOC

![moc logo](./images/moc.svg)

**MOC** is our ongoing attempt at building a *Medium Online Course* on
using `R` for data analysis in psychology and related fields.

It is consists of an online book with exercises and other accompanying
materials.

In the future we can expand it to feature a study plan and/or video
tutorials/classes, etc...

**To read what we have written so far you can head over to the [book's
page](https://rggd.gitlab.io/moc/).**

**To collaborate on this project you can head over to its [GitLab
project page](https://gitlab.com/rggd/moc).**



## SciOps

![SciOps logo](./images/sciops.svg)

**SciOps** is our ongoing attempt to create a template (or templates)
to help researchers leverage cutting-edge tools to increase their
productivity, by automating repetitive tasks, as well as their
research's reproducibility and transparency.

These tools are already being used today by programmers, be it on the
largest of organizations or on the smallest teams, to do what is labeled
CI/CD (continuous integration and continuous delivery). However, their
use in science is still quite rare...and that is what we aim to change
with **SciOps**.

Currently the prototype template is capable of automatically running `R`
data analysis scripts, in the cloud (automatically installing and
running the required packages in a `Docker` container), as well as
compile an academic paper draft and conference slides using `RMarkdown`.

**To collaborate on this project you can head over to its [GitLab
project page](https://gitlab.com/rggd/SciOps).**

**To learn about this project you can checkout [these slides from
a workshop that mentioned
it](https://rggd.gitlab.io/sciops_workshop/slides), but you're probably
better of asking about it in our discord, mailing list, or reaching out
to
[joao.filipe.santos@campus.ul.pt](mailto:joao.filipe.santos@campus.ul.pt)
directly.**



## Website

This website itself is an ongoing effort on organizing our resources and
letting people know about what we do at RUGGED.

The website is compiled from plain `markdown` files with some custom
`HTML`, `CSS`, and image files. It is hosted and automatically rendered
by GitLab as a GitLab Pages' page, using [`make
it_stop`](https://gitlab.com/joao-o-santos/make-it-stop) (a static site
generator developed by one of our members---João O. Santos).

**To collaborate on this project you can head over to its [GitLab
project page](https://gitlab.com/rggd/rggd.gitlab.io).**

**To take a look at the website...well you're already doing that
so...just carry on browsing to see what we have done so far.**



## Contributing

To contribute to our projects you can head on over to [our GitLab
group](https:///gitlab.com/rggd) and look through the list. There's an
entry for each project featured on this list.

If you are not part of our [GitLab group](https://gitlab.com/rggd), but
you have a GitLab account, you can go to our
[group](https://gitlab.com/rggd) and ask to be invited. If you don't
have a [GitLab](https://gitlab.com) account you can
create one [here](https://gitlab.com/users/sign_up).

**Importantly, you do not have to be very experienced with `R`, `git`,
`GitLab` or the other tools we use to contribute. There are less
technical ways to help, for instance, commenting on discussions, giving
feedback and suggestions, helping translate resources, etc... You can
take a look at the [open issues labeled "good first
issue"](https://gitlab.com/groups/rggd/-/issues/?sort=created_date&state=opened&label_name%5B%5D=good%20first%20issue&first_page_size=100),
for...well...a "good first issue" to get you started contributing to our
projects.**

If you're more comfortable with projects and/or tooling but don't know
how you can help , you can take a look at the [open issues labeled "help
wanted"](https://gitlab.com/groups/rggd/-/issues/?sort=created_date&state=opened&label_name%5B%5D=help%20wanted&first_page_size=100)
for...well...a list of issues where help is wanted.

**Please note that by contributing you're signing the DCO (Developer
Certificate of Origin). Meaning, you're stating that you have the legal
right to make the contribution (the contribution was made by you, or you
otherwise have the right to submit it) under the applicable project's
license/s. Also note that, unfortunately, right now, we do not have
the means for paying for your time/work.**
