# Rules


### First Rule

**Do not share confidential information** (e.g., datasets with
participants' personally identifiable information), **or ask for
solutions to class assignments.**


### Second Rule

**Respect everybody. Don't reveal the identity of group members who want
to remain anonymous, tolerate opposing views, keep the discussion
civil. Be nice whenever possible, except if doing so infringes on the
First Rule.** Doing other people's assignments, or mentioning their name
when they want to stay anonymous is not nice.


### Third Rule

**Respect the moderators' requests except if doing infringes on the
First or Second Rules.** Spoiler alert: the moderators are not going to
ask you to break any of the above rules.
