# Procedures

Having standardize procedures streamlines cooperation and the flow of
our sessions. Please, read the procedures below carefully before
participating in a RUGGED session.

It is normal for some procedures to seem weird or needlessly complex, do
not worry, you do not have to master the procedures or the tooling to
participate in a RUGGED session. We simply ask you to read them and make
some effort to comply. If you have any questions regarding the
procedures, as well as any suggestions for changing them, feel free to
open an issue, and/or submit a merge request, and/or discuss it in our
discourse server.


## File Sharing

### [GitLab.com/rggd](https://gitlab.com/rggd)

- There is a RUGGED group over at
  [gitlab.com/rggd](https://gitlab.com/rggd) to host, and help us
  collaborate on projects.
- **The group is public, but it can host private projects.**
- Please contact the maintainers if you wish to be added to the group.
- [GitLab](https://gitlab.com) is platform for hosting and collaborating
  on projects using `git`. It also adds extra functionality, such as
  automatically running and testing your scripts, or hosting [GitLab
  Pages](https://gitlab.com) such as this one.
- `git` is a tool that allows us to keep track of multiple file, and
  directories (aka folders), their changes and versions. It is
  particularly useful when collaborating on code projects, such as
  multiple R scripts.
- If you're not familiar with [GitLab](https://gitlab.com) or `git`, and
  you're feeling brave, try following the [GitLab
  tutorials](https://docs.gitlab.com/ee/tutorials/#use-git).

### [Wormhole.app](https://wormhole.app/)

- We use the [wormhole.app](https://wormhole.app) to share more
  sensitive files, or files you do not wish to track with `git`.
- This tool allows you to limit the number of allowed downloads and the
  expiration date, for each link you generate, which might be
  particularly useful if the file are somewhat sensitive.


## Coding Style

- **Please try to follow the data importing recommendations suggested in
  our [coding style](https://rggd.gitlab.io/coding_style.html).**
- Try and keep a consistent coding style, but do not worry too much
  about it if you are just starting with R.


## Asking for Help

- **When you ask for help in our discord forum, avoid taking pictures or
  sharing screenshots of your code.**
- **Instead, try and write the lines of code you have questions about in
  a code block.**
- To write a code block in discord follow the instructions below (or
  google how to do it).
- Still, if you cannot figure out how to write a code block do not let
  that discourage you from asking for help in our discord server.

<pre class = code-block>
```
# Writing three consecutive backticks will open a code block.
# The block can span to several lines of code.
print("Code block example")

# Writing another three consecutive backticks will close the code block.
```
</pre>
