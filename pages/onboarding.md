<!-- 1 MENU_ENTRY=Onboarding -->
# Onboarding

**Thank you for your interest in joining RUGGED!**



## TL;DR (Too Long Didn't Read):

1. Read and follow the rules

2. Join the platforms:
	- <a href="https://groups.google.com/g/rugged_mailing_list/" target="_blank">Click here and ask to join our mailing list.</a>
	- <a href="https://discord.gg/E8mdf9CYMC" target="_blank">Click this invitation link to join our discord server.</a>
	- <a href="https://gitlab.com/rggd" target="_blank">Click here and ask to join our GitLab group.</a>

3. Introduce yourself in the [discord server](https://discord.com/channels/774427744108544091/774436262245171211)

4. Participate in an episode/session

5. Get added to the members page



## Step 1: Read and Follow the Rules

**Please read through our rules, making sure you understand and agree to
follow them.**

If you have any questions feel free to email
[joao.filipe.santos@campus.ul.pt](mailto:joao.filipe.santos@campus.ul.pt).


### First Rule

**Do not share confidential information** (e.g., datasets with
participants' personally identifiable information), **or ask for
solutions to class assignments.**


### Second Rule

**Respect everybody. Don't reveal the identity of group members who want
to remain anonymous, tolerate opposing views, keep the discussion
civil. Be nice whenever possible, except if doing so infringes on the
First Rule.** Doing other people's assignments, or mentioning their name
when they want to stay anonymous is not nice.


### Third Rule

**Respect the moderators' requests except if doing infringes on the
First or Second Rules.** Spoiler alert: the moderators are not going to
ask you to break any of the above rules.



## Step 2: Join the Platforms


### Mailing List

<a href="https://groups.google.com/g/rugged_mailing_list/" target="_blank">Click here and ask to join our mailing list.</a>

*Note: Once you're in the mailing list you can send an email to all
  members by addressing it to
  [rugged\_mailing\_list@googlegroups.com](mailto:rugged_mailing_list@googlegroups.com)*


### Discord

<a href="https://discord.gg/E8mdf9CYMC" target="_blank">Click this invitation link to join our discord server.</a>

*Note: If you don't have a discord account, you will have to create
one.*


### GitLab

<a href="https://gitlab.com/rggd" target="_blank">Click here and ask to join our GitLab group.</a>

*Note: If you don't have a GitLab account, you will have to [create
one](https://gitlab.com/users/sign_up).*


### Asking for Help

**Should you encounter any problems you can ask for help in our discord
server. If you're having problems with discord you can email the mailing
list instead. In case you can't join the mailing list nor discord, you
can send an email with your questions to
[joao.filipe.santos@campus.ul.pt](mailto:joao.filipe.santos@campus.ul.pt).**



## Step 3: Introduce Yourself

Now that you have joined our discord server, you can go over to the
[#introduce\_yourself
channel](https://discord.com/channels/774427744108544091/774436262245171211)
and tell us a bit about yourself.



## Step 4: Participate in a Episode/Session

Checkout out our [episode guide](./episode_guide.html) to see our
upcoming episodes.

To participate on an episode, just go to the [Session voice
channel](https://discord.com/channels/774427744108544091/774427744598491210)
when the episode goes live.



## Step 5: Get Added to the Members Page

Now that you are a part of RUGGED, should you wish to be featured on our
[members page](./members.html), send us a photo and a short biography
(three to five sentences) so we can add you to the page.

You can send the photo and bio through any of the platforms or directly
to
[joao.filipe.santos@campus.ul.pt](mailto:joao.filipe.santos@campus.ul.pt)



## Step 6: Contribute to Our Projects

To contribute to our projects you can look through [the project
list](./projects.html), and then head over to [our GitLab
group](https:///gitlab.com/rggd) and open the corresponding project
entry.

Once there (if you're logged in) you can comment on issues, open new
ones, and submit merge requests.

**Importantly, you do not have to be very experienced with `R`, `git`,
`GitLab` or the other tools we use to contribute. There are less
technical ways to help, for instance, commenting on discussions, giving
feedback and suggestions, helping translate resources, etc... You can
take a look at the [open issues labeled "good first
issue"](https://gitlab.com/groups/rggd/-/issues/?sort=created_date&state=opened&label_name%5B%5D=good%20first%20issue&first_page_size=100),
for...well...a "good first issue" to get you started contributing to our
projects.**

If you're more comfortable with projects and/or tooling but don't know
how you can help you can take a look at the [open issues labeled "help
wanted"](https://gitlab.com/groups/rggd/-/issues/?sort=created_date&state=opened&label_name%5B%5D=help%20wanted&first_page_size=100)
for...well...a list of issues where help is wanted.

**Please note that by contributing you're signing the DCO (Developer
Certificate of Origin). Meaning, you're stating that you have the legal
right to make the contribution (the contribution was made by you, or you
otherwise have the right to submit it) under the applicable project's
license/s. Also note that, unfortunately, right now, we do not have
the means for paying for your time/work.**
