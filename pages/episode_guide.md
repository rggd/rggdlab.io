<!-- 2 MENU_ENTRY=Episode Guide -->
# Episode Guide

Please [sign-up](./onboarding.html) for RUGGED's [discord
server](https://dicord.gg/E8mdf9CYMC) and [mailing
list](https://groups.google.com/g/rugged_mailing_list/) so you can be
notified of important updates.


## Season 0 (2021):

- Episode 00 (E00):
	+ Group's goals: creating a forum for sharing questions,
	  answers, and resources on `R`, as well as setting up a regular
	  meeting.
	+ How to structure the sessions: we would try to adapt the
	  format of Barbar Sarnecka's WriteOn writing workshop.
	+ Picking a platform: [discord.com](https://discord.com)
	+ Choosing a name: RUGGED!
- E01:
	+ Ice breaker
	+ Exercises + sharing/discussing solutions
	+ Reviewing how the meeting went and how to improve.
- E02: Same structure as E01.
- E03: Same structure as E01 and E02.



## Season 1 (2021-2022):

- Episode 00 (E00):
	+ Discussion of our goals for that year.
	+ Decision start learning `R` from the ground up.
- E01: Language Basics I
- E02: Language Basics II
- E03: Datasets (`data.frame`s) I
- E04: Datasets (`data.frame`s) II - Data Importing and Cleaning
- E05: Datasets III - Data Reformatting/Wrangling
- E06: Linear Models I - The Model Comparison Approach (Judd et al.)
- E07: Linear Models II - Practice Using `lm()` and `afex::aov_car()`
- E08: Linear mixed/mutilevel models I - Approaches to Repeated Measures
- E08: Linear mixed/mutilevel models II - More Complex Models
- E10: Autonomous work, Organizing Projects and Files, Planning the
  Summer Break and When to Restart



## Season 2 (2022-2023):

- Episode 00 (E00):
	+ Discussion of if and how the group could continue.
	+ Turns out RUGGED is alive and well!
	+ Brainstorming of ideas for Season 2.
- E01 - October 31st - 10h30m - Mafalda Mascarenhas
- E02 - November 14th - 10h30m - André Vaz
- E03 - November 28th - 10h30m - Mariona Pascual Peñas
- E04 - December 12th - 10h30m - João O. Santos + Housekeeping + R&D
- **Christmas break: From December 13th until January 8th**
- E06 - January 9th - 10h30m - Canceled
- E07 - January 23rd - 10h30m - Onboarding members onto RUGGED projects
  (including a GitLab overview)
- E08 - February 6th - 10h30m - Mariona Pascual Peñas
- **Carnival break: From February 7th until February 26th**
- E09 - February 27th - 10h30m - Introducing the [QHELP
  project](https://www.qhelp.eu), and the [QHELP 2023 mobility
  event](https://qhelp-fp.gitlab.io/arrifana).
- E10 - March 13th - 10h30m - Introduction to `R/Shiny` Apps:
	+ `ui` and `server` components.
	+ Breaking down the code for [moc's apps](https://joao-o-santos.shinyapps.io/intercepts_and_slopes).
	+ Making the case for creating custom functions, and sticking to
	  `observeEvent()`, to simplify the code in `sever`.
- **Easter break: From March 14th until April 9th**
- E11 - April 10th - 12h30m - Brainstorming RUGGED special events and
  ways to get more attendance on our episodes.
- **E12 - April 28th - 15h00m - The first ever RUGGED Open Day!**
- **E13 - June 1st - Mariona Pascual Peñas + Season wrap-up party**



## Season 3 (2023-2024):

- Episode 00 (E00):
	+ State of the union: how we are, what we have been doing, and
	  what we do we want do.
	+ Brainstorming for Season 3.
	+ This season we'll dedicate the first part of each episode to
	  work on our projects. On the second part someone will present
	  something they're working on, and/or ask for help with
	  something.
- E01 - October 20th - 16h00m - Marta Granadeiro:
	+ Linear mixed models
	+ data from a pilot study of stimuli
- E02 - November 6th - 16h00m - Cristina Mendonça:
	+ Presentation of Lackner and collaborator (2023; of which
	  Cristina is a co-author), with simulations in R.
- E03 - November 17th - 16h00m - Laura Mealha: Masters thesis data and
  reflections on learning R
- E04 - December 11th - 16h00m - Mariona Pascual Peñas
- E05 - February 9th - 15h00m: Reconnecting and discussion on how
  informal RUGGED presentations can be, and how imposter syndrome is
  prevalent.
- E06 - February 23rd - 16h00m:
	+ Mariona Pascual Peñas: Questions on how to compute chronback's
	  alpha in R
	+ Cristina Mendonça: Generating not so random dot matrices
	+ Mariona Pascual Peñas: Research with smart pens
	+ Marta Granadeiro: Questions on factorial designs with repeated
	  measures, and their analysis with linear mixed models
- E07 - March 8th - 16h00m - Laura Mealha: Repeated measures ANOVAs


Please sign-up on both RUGGED's [discord
server](https://dicord.gg/E8mdf9CYMC) and [mailing
list](https://groups.google.com/g/rugged_mailing_list/) so you can be
notified of important updates.
