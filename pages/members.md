<!-- 4 MENU_ENTRY=Members -->

# Members



## Afonso Rafael

![Afonso Rafael's picture](./images/ar.png)

I'm Afonso Rafael and according to João O. Santos I'm RUGGED's first
real programmer! Currently, I am working in the Barcelona's
Supercomputing Center on the compilers team, helping improve the
[LLVM](https://llvm.org/)'s Infrastructure. My professional interest is
compiler design and infrastructures, and I really love building tools
to help others do their work. I am a polyglot, but the languages I know
are mainly "formal" ones... by that I mean programming languages
basically. I like R as a programming language, and also as a statics
tool. I am a strong apologist of the idea that R and/or python should be
taught in any social science degree! If by any reason you want to
contact me or know more about me, here is my web page:
[alf0nso.github.io](https://alf0nso.github.io/)


## André Vaz

![Andre Vaz's picture](./images/av.jpg)

I'm André, a fourth year PhD student of social psychology at the
University of Lisbon. My research mostly focuses on moral judgments, in
terms of how people perceive and make judgments about others and
themselves. I also do some research in judgment and decision-making.
Beyond academia, I'm an enthusiast about fantasy literature and all
things animal.

<!--
TODO: decide whether to ask people for this info or if we delete it
Interests: Moral Judgment; JDM; Self-enhancement
-->



## Cristina Mendonça

![Cristina Mendonça's picture](./images/cm.jpg)

I am Cristina Mendonça and I received my PhD in Social Psychology from
the inter-university doctoral program LiSP – Lisbon PhD in Social
Psychology. I am currently a post-doctoral researcher at LIP
– Laboratório de Instrumentação e Física Experimental de Partículas, an
invited professor at Universidade Católica Portuguesa, and the
co-principal investigator of an FCT-funded project on how people think
about others' prejudice. My main research interest is in judgment and
decision-making and how it is affected by and/or affects social factors
and motivation. I prefer my statistics in R and enjoy building shiny
apps and other interactive teaching methods.

<!--
TODO: decide whether to ask people for this info or if we delete it
[Google Scholar](https://scholar.google.com/citations?user=Ht-9DwYAAAAJ)
[ORCID](http://www.cienciavitae.pt//DD12-832A-9096)
-->



## Emerson Do Bú

![Emerson Do Bú's picture](./images/edb.png)

I am a Ph.D. Candidate at the Faculty of Psychology and the Institute of
Social Sciences at the University of Lisbon. I am also a Visiting
Scholar at Virginia Commonwealth University. My doctoral research
focuses on time investment bias and its mechanisms in medical
decision-making. I am particularly interested in studying healthcare
inequalities and hope to develop research in the coming years that will
provide more information on interventions for socially disadvantaged
groups. I joined RUGGED to expand my knowledge of the R programming
language. I have been impressed by the group's expertise and am grateful
for the support they provide in areas such as data analysis and
programming. I am excited to continue learning and growing with this
supportive community.



## Francisco Cruz

![Francisco Cruz's picture](./images/fc.png)

I'm Francisco, a PhD student on social psychology. My research interests
are diverse: though my thesis will be on whether people see psychology
as a science or not, relative to other sciences and on its own, I have
been working on face and word processing for a couple of years now.
Other projects I am involved in focus on how we perceive homogeneity in
social groups across time and on why we endorse the claims of others
even when they are talking about domains beyond those of their
expertise. I am keen on both the conceptual, such as a more
philosophical take on psychology, and practical, such as statistics and
data analysis, aspects of our science. Pleased to meet you!



## João O. Santos

![João O. Santos' picture](./images/jos.png)

I'm João (John), a social psychology PhD student, as well as
a statistics/`R` programming consultant and tutor, with a great interest
in connecting philosophy of science, research methods, and data
analysis. My research has focused on how adults perceive children, as
a social category. My passion for programming and IT has led me to my
freelance work, where I leverage technology to solve problems and
automate tasks, in academia and in the industry.



## María Jesús Maraver

![María Jesús Maraver's picture](./images/mjm.jpg)

I'm María Jesús Maraver, postdoctoral researcher in cognitive
neuroscience at the Mind, Brain, and Behavior Research Center of the
University of Granada, Spain. In recent years, my work has focused on
the study of memory error correction processes and learning enhancement.
I am committed to the importance of bringing scientific knowledge to the
general public and often participate in scientific dissemination
activities. I collaborate with Women in Cognitive Science in Europe
(E-WiCS) and in my free time, I love to cook, read, and give visibility
to people living with type one diabetes.



## Mariona Pascual Peñas

![Mariona Pascual Peñas' picture](./images/mpp.jpg)

I'm Mariona Pascual, a postdoctoral researcher at the University of
Lisbon. My research mostly focuses on writing and reading difficulties,
and their neuropsychological correlates. I aim to contribute to
education and psychological sciences with empirically-based research, to
improve and maximize the impact and effectiveness of writing and reading
instruction. In my free time, I'm a foodie, and I enjoy rowing and
dancing.



## Vitória Melita

![Vitória Melita's picture](./images/vm.jpg)

Hi! I’m Vitória and I’m in the second year of my degree in Psychology at
the University of Lisbon. I’m particularly keen on social and cognitive
psychology, but still exploring the field. I could describe myself as
a bookworm (specially fantasy and “classics”), food lover and as someone
who really likes hugs.
