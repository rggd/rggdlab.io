<!-- 0 MENU_ENTRY=Home -->
# Home

**Welcome to the RUGGED website!**



## Who Are We?

We are a group of `R` users mostly studying or working in psychology and
related fields. Our personal research interests are quite diverse, the
common thread is that we end up having some sort of quantitative data
that we try to analyze in `R`. Our levels of experience and familiarity
with `R` are also very different, as we accommodate people of all
experience levels.

**As long as you are willing to try to learn `R`, and you follow the
rules, you're welcome to [join us!](./onboarding.html)**



## What's In a Name

**RUGGED** stands for `R` Users Grouped.

Sometimes using `R` and doing statistical analysis can be tough...in
those times, us `R` users need to stick together and be **RUGGED**!



## Join Us!

**If you want to join RUGGED simply follow the instructions in the
[Onboarding](./onboarding.html) page.**

**We're glad to have you!**
