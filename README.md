<!-- SPDX-License-Identifier: CC0-1.0 -->
# RUGGED

**Welcome to the RUGGED website's GitLab project page!**

## Description

In this repository you'll find all the files needed to generate and
deploy our website as GitLab Pages' page.

To make changes to the website's pages you probably only need to edit
the corresponding page in the `pages/` directory. If you intend on
making stylistic changes you might need to edit the
`templates/styles.css` file (please update the copyright notice on top,
if you do).

## Asking for Help

**If the explanations above or below sound too complex for you, please
do not be discouraged from contributing. You can ask for help in our
discord server, in the mailing list, and we'll help you get started.**

## Contributing

1. Write your pages in `PAGES_DIR`

2. Customize the files in `TEMPLATES_DIR`

3. If necessary make change the defaults in `config.mk`

4. Run `make it_stop`.
	- *Note: this requires having the programs `make it_stop`
	  depends on installed on your machine. If you're on `Linux`,
	  `BSD`, `MacOS`, or other UNIX-like operating systems, chances
	  are you probably have all the required programs installed
	  already. If you're on `Windows` you probably better off
	  [installing the `Windows Subsystem for Linux
	  (WSL)](https://learn.microsoft.com/en-us/windows/wsl/install).
	  Regardless, even if you can't preview the changes on your
	  machine, if you commit and push them, GitLab will
	  automatically regenerate the pages (as long the CI/CD pipeline
	  doesn't fail). If you just changed some text in a page, don't
	  be afraid to commit it without previewing it, especially if
	  you don't have the required programs installed.*

5. Preview the rendered pages in `WEB_DIR` by opening them with your
   browser.
	- *Note: Again, if you just changed some text in a page, don't
	  be afraid to commit it without previewing it, especially if
	  you don't have the required programs installed.*

6. Iterate over the previous steps until you're happy with the results

7. `git commit` and `git push` your changes and GitLab will
   automatically regenerate and deploy the website (as long as the CI/CD
   pipeline doesn't fail).

**For more detailed instructions checkout the official
[make it_stop tutorial](https://joao-o-santos.gitlab.io/make-it-stop/tutorial.html).**
